package com.communere.android.ui.signup

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.communere.android.R
import com.communere.android.databinding.FragmentSignupBinding
import com.communere.android.di.Injectable
import com.communere.android.utils.hasPermission
import com.communere.android.utils.openGallery
import com.communere.android.utils.showSnack
import javax.inject.Inject

/**
 * This fragment provides and user interface for Signup process
 * @property viewModel, related viewmodel to this fragment
 * @property mBinding, related ui to this fragment
 */
class SignupFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: SignupViewModel by viewModels { viewModelFactory }

    private lateinit var mBinding: FragmentSignupBinding

    companion object {
        const val GALLERY_Intent = 100
        const val STORAGE_PERMISSION = 200
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false)
        context ?: return mBinding.root

        mBinding.apply {

            // For choosing an image for user from gallery or file manager
            imageUser.setOnClickListener {

                // Request runtime permission for access to storage
                hasPermission(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    STORAGE_PERMISSION
                ) {
                    openGallery(GALLERY_Intent)
                }
            }

            // Signup
            btnSignup.setOnClickListener {
                viewModel.signup(
                    textInputFullname.text.toString(),
                    textInputEmail.text.toString(),
                    textInputPassword.text.toString(),
                    textInputConfirmPassword.text.toString(),
                    btnIsAdmin.isChecked
                )
            }
        }

        subscribe()

        return mBinding.root
    }

    /**
     * Subscribe view model
     */
    private fun subscribe() {
        viewModel.message.observe(viewLifecycleOwner, Observer {
            mBinding.root.showSnack(it)
        })

        viewModel.isRegistered.observe(viewLifecycleOwner, Observer { isRegistered ->
            if (isRegistered) {
                // pop back stack to [LoginFragment]
                findNavController().popBackStack()
            }
        })

        viewModel.image.observe(viewLifecycleOwner, Observer {
            mBinding.apply {
                image = it
                executePendingBindings()
            }
        })
    }

    /**
     * Handles result of [GALLERY_Intent]
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (data != null && requestCode == GALLERY_Intent) {

                // Image selected by user
                viewModel.onImageSelected(data.data)
            }
        }
    }

    /**
     * Handles result of requesting runtime permission for [STORAGE_PERMISSION]
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {

                // User granted access to storage permission
                openGallery(GALLERY_Intent)

            } else {

                // Permission denied
                mBinding.root.showSnack(R.string.msg_access_to_storage_not_permitted)

            }
        }
    }

}
