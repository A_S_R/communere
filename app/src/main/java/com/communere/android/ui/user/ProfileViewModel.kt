package com.communere.android.ui.user

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.communere.android.R
import com.communere.android.di.CoroutineScopeIO
import com.communere.android.data.model.Role
import com.communere.android.data.model.User
import com.communere.android.data.repository.UserRepository
import com.communere.android.utils.RealPathUtil
import com.communere.android.utils.compressImageAndCopyToPics
import com.communere.android.utils.getRandomString
import com.communere.android.utils.isValidEmail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import java.io.File
import javax.inject.Inject

/**
 * Connection between fragment and repository
 * @property context, for getting real path of selected image file and compressing it
 * @property repository, for providing data
 */
class ProfileViewModel @Inject constructor(
    private val context: Context,
    private val repository: UserRepository,
    @CoroutineScopeIO private val ioScope: CoroutineScope
) : ViewModel() {

    // For keeping instance of logged in user
    lateinit var loggedInUser: User

    // For keeping instance of selected user
    lateinit var selectedUser: User

    // Provides delete permission based on the loggedIn user and the selected user
    val hasDeletePermission get() = loggedInUser.role == Role.ADMIN || loggedInUser.id == selectedUser.id

    // Provides update permission based on the logged in user and the selected user
    val hasUpdatePermission get() = loggedInUser.id == selectedUser.id

    // For showing messages to user
    private val _message = MutableLiveData<Int>()
    val message: LiveData<Int> = _message

    // For selecting user's info from table
    private val _userId = MutableLiveData<Int>()
    fun postUserId(id: Int) {
        _userId.postValue(id)
    }

    // For observing user's info selected from table based on user's unique id
    val observableUser: LiveData<User> = Transformations.switchMap(_userId) {
        repository.getUserById(it)
    }

    fun update(fullName: String, email: String, onUpdatedSuccessfully: () -> Unit) {

        // Check email validation
        if (!email.isValidEmail()) {
            _message.postValue(R.string.msg_email_input_is_not_valid)
            return
        }

        // Update transaction
        repository.update(selectedUser.apply {
            this.fullName = fullName
            this.email = email
        }, ioScope)

        // Fire, update transaction is done
        onUpdatedSuccessfully()
    }

    fun delete(onDeletedSuccessfully: () -> Unit) {
        // Delete transaction
        repository.delete(selectedUser, ioScope)

        // Fire, delete transaction is done
        onDeletedSuccessfully()
    }

    /**
     * When user selected a file from intent.
     * Compress the file and move it to Pics dir.
     * @param data, given by intent
     */
    fun onImageSelected(data: Uri?) {
        // Init image property inside model
        val image = "${getRandomString()}.jpg"

        // Get image's file from data
        val actualImage = File(RealPathUtil.getRealPath(context, data))

        // compress image to lower size
        context.compressImageAndCopyToPics(actualImage, image)

        // Save to DB
        repository.update(selectedUser.apply {
            this.image = image
        }, ioScope)
    }

    /**
     * Cancel any process is running now
     */
    override fun onCleared() {
        ioScope.cancel()
        super.onCleared()
    }
}