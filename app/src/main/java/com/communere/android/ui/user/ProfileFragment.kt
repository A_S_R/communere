package com.communere.android.ui.user

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.communere.android.R
import com.communere.android.databinding.FragmentUserProfileBinding
import com.communere.android.di.Injectable
import com.communere.android.utils.hasPermission
import com.communere.android.utils.openGallery
import com.communere.android.utils.showSnack
import javax.inject.Inject

/**
 * This fragment shows profile of the user
 * @property viewModel, related view model to this fragment
 * @property mBinding, related layout (UI)
 * @property args, arguments passed to this fragment
 */
class ProfileFragment : Fragment(), Injectable {

    companion object {
        const val GALLERY_Intent = 100
        const val STORAGE_PERMISSION = 200
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ProfileViewModel by viewModels { viewModelFactory }

    private lateinit var mBinding: FragmentUserProfileBinding

    private val args: ProfileFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_profile, container, false)
        context ?: return mBinding.root

        // Pass args to view model for getting data
        viewModel.apply {
            loggedInUser = args.loggedInUser
            selectedUser = args.selectedUser
            postUserId(selectedUser.id!!)
        }

        mBinding.apply {

            // Choose image by clicking this button
            imageUser.setOnClickListener {

                // Request runtime permission for access to storage
                hasPermission(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    STORAGE_PERMISSION
                ) {
                    openGallery(GALLERY_Intent)
                }
            }

            // Update the user's info
            btnUpdate.setOnClickListener {
                viewModel.update(
                    textInputFullname.text.toString(),
                    textInputEmail.text.toString()
                ) {
                    // on Update is done
                    mBinding.root.showSnack(R.string.msg_updated_successfully)
                }
            }

            // Delete the user
            btnDelete.setOnClickListener {
                viewModel.delete {
                    // After delete, we navigate to [UsersFragment]
                    findNavController().popBackStack()
                }
            }

            // For hiding and showing features of this page
            hasDeletePermission = viewModel.hasDeletePermission
            hasUpdatePermission = viewModel.hasUpdatePermission
        }

        subscribe()

        return mBinding.root
    }

    /**
     * Subscribe view model
     */
    private fun subscribe() {

        // If any property of user changed, we need to update UI
        viewModel.observableUser.observe(viewLifecycleOwner, Observer {

            viewModel.selectedUser = it

            mBinding.apply {
                user = it
                executePendingBindings()
            }
        })

        // Observing any messages come from viewmodel
        viewModel.message.observe(viewLifecycleOwner, Observer {
            mBinding.root.showSnack(it)
        })
    }

    /**
     * Handles result of Gallery intent
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (data != null && requestCode == GALLERY_Intent) {

                // Image selected by user
                viewModel.onImageSelected(data.data)
            }
        }
    }

    /**
     * Handles result of requested permissions
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {

                // User granted access to storage permission
                openGallery(GALLERY_Intent)

            } else {

                // Permission denied
                mBinding.root.showSnack(R.string.msg_access_to_storage_not_permitted)

            }
        }
    }

}
