package com.communere.android.ui.user

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.communere.android.databinding.ItemUserBinding
import com.communere.android.ui.base.ListAdapter
import com.communere.android.data.model.User

/**
 * List adapter for recyclerview in [UsersFragment]
 * @property hasDeletePermission, is needed for showing or hiding delete button
 * @property onItemClicked, fired when item is selected
 * @property onDeleteClicked, fired when delete button is clicked
 */
class UsersAdapter(
    private val hasDeletePermission: Boolean,
    private val onItemClicked: (user: User) -> Unit,
    private val onDeleteClicked: (user: User) -> Unit
) : ListAdapter<User, UsersAdapter.ViewHolder>(
    DiffCallback()
) {

    inner class ViewHolder(
        private val mBinding: ItemUserBinding
    ) : RecyclerView.ViewHolder(mBinding.root) {

        init {

            mBinding.root.setOnClickListener {
                onItemClicked(getItem(bindingAdapterPosition))
            }

            mBinding.btnDelete.setOnClickListener {
                onDeleteClicked(getItem(bindingAdapterPosition))
            }
        }

        fun bind(item: User) {
            mBinding.apply {
                user = item
                hasDeletePermission = this@UsersAdapter.hasDeletePermission
                executePendingBindings()
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<User>() {
        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }

        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            val item = getItem(position)
            bind(item)
            itemView.tag = item
        }
    }
}