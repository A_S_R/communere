package com.communere.android.ui.base

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

/**
 * [ListAdapter] provided by recyclerview library
 * can not detect changes on existing model's properties,
 * that's why we create this class
 */
abstract class ListAdapter<T, VH : RecyclerView.ViewHolder>(
    callback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, VH>(callback) {

    override fun submitList(list: List<T>?) {
        if (list?.count() == currentList.count()) {
            super.submitList(null)
        }
        super.submitList(list)
    }

}