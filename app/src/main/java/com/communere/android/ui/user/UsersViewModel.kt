package com.communere.android.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.communere.android.di.CoroutineScopeIO
import com.communere.android.data.model.Role
import com.communere.android.data.model.User
import com.communere.android.data.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

/**
 * Interface between fragment and repository
 * @property repository, provides data to this viewmodel
 */
class UsersViewModel @Inject constructor(
    private val repository: UserRepository,
    @CoroutineScopeIO private val ioScope: CoroutineScope
) : ViewModel() {

    // Keep a reference to loggedIn user
    var loggedInUser: User? = null

    // Provides delete permission based on loggedIn user's role
    val hasDeletePermission
        get() = loggedInUser?.role == Role.ADMIN


    val users: LiveData<List<User>> = repository.users

    fun delete(user: User) {
        repository.delete(user, ioScope)
    }
}