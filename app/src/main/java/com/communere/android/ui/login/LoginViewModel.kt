package com.communere.android.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.communere.android.R
import com.communere.android.data.repository.LoginRepository
import com.communere.android.data.model.User
import com.communere.android.utils.isValidEmail
import javax.inject.Inject

/**
 * Connection between LoginFragment and LoginRepository
 * @property repository, data repository
 */
class LoginViewModel @Inject constructor(private val repository: LoginRepository) : ViewModel() {

    companion object {
        const val EMAIL = "email"
        const val PASS = "password"
    }

    // For showing messages to user
    private val _message = MutableLiveData<Int>()
    val message: LiveData<Int> = _message

    // For posting parameters for login
    private val _loginParams = MutableLiveData<Map<String, String>>()

    fun login(email: String, password: String) {

        // For email validation
        if (!email.isValidEmail()) {
            _message.postValue(R.string.msg_email_input_is_not_valid)
            return
        }

        _loginParams.postValue(mutableMapOf<String, String>().apply {
            put(EMAIL, email)
            put(PASS, password)
        })
    }

    // For observing user is found or not
    val user: LiveData<User> = Transformations.switchMap(_loginParams) {
        repository.login(it[EMAIL], it[PASS])
    }

}