package com.communere.android.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.communere.android.R
import com.communere.android.databinding.FragmentLoginBinding
import com.communere.android.di.Injectable
import com.communere.android.utils.showSnack
import javax.inject.Inject

/**
 * This fragment provide an user interface for Login process
 * @property viewModel, an instance of [LoginViewModel]
 * @property mBinding, layout for this fragment
 */
class LoginFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: LoginViewModel by viewModels { viewModelFactory }

    private lateinit var mBinding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        context ?: return mBinding.root

        mBinding.apply {

            // Login user by clicking this button
            btnLogin.setOnClickListener {
                viewModel.login(
                    textInputEmail.text.toString(),
                    textInputPassword.text.toString()
                )
            }

            // Navigate to Singup page
            btnSignup.setOnClickListener {
                findNavController().navigate(LoginFragmentDirections.actionSignupDest())
            }
        }

        subscribe()

        return mBinding.root
    }

    /**
     * Subscribe view model
     */
    private fun subscribe() {
        viewModel.user.observe(viewLifecycleOwner, Observer { user ->
            // Check user has been found or not
            if (user == null) {
                mBinding.root.showSnack(R.string.msg_user_not_found)
            } else {
                mBinding.root.showSnack(R.string.msg_logged_in_successfully)

                // Navigate to [UsersFragment]
                findNavController().navigate(
                    LoginFragmentDirections.actionUsersDest(
                        user
                    )
                )
            }
        })

        viewModel.message.observe(viewLifecycleOwner, Observer {
            mBinding.root.showSnack(it)
        })
    }

}
