package com.communere.android.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.communere.android.R
import com.communere.android.databinding.FragmentUsersBinding
import com.communere.android.di.Injectable
import javax.inject.Inject

/**
 * This fragment shows list of users
 * @property viewModel, related view model to this fragment
 * @property mBinding, related layout (UI)
 * @property args, arguments passed to this fragment
 * @property adapter of recyclerview
 */
class UsersFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: UsersViewModel by viewModels { viewModelFactory }

    private lateinit var mBinding: FragmentUsersBinding

    private val args: UsersFragmentArgs by navArgs()

    private val adapter by lazy {

        // listeners implemented by lambda
        UsersAdapter(viewModel.hasDeletePermission,
            onItemClicked = { user ->

                findNavController().navigate(
                    UsersFragmentDirections.actionUserProfileDest(
                        args.loggedInUser,
                        user
                    )
                )

            }, onDeleteClicked = { user ->

                viewModel.delete(user)

            })
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_users, container, false)
        context ?: return mBinding.root

        // This callback will only be called when fragment is at least Started.
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    // Handle the back button event
                    activity?.finishAffinity()
                }
            }
        // Because we need to finish the maintainer activity
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        // Pass args to viewmodel for getting permission
        viewModel.apply {
            loggedInUser = args.loggedInUser
        }

        mBinding.apply {
            recyclerView.adapter = adapter
        }

        subscribe()

        return mBinding.root
    }

    /**
     * Subscribe viewmodel
     */
    private fun subscribe() {
        viewModel.users.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)

            // For showing that list is empty or not
            mBinding.apply {
                isEmpty = it.isNullOrEmpty()
                executePendingBindings()
            }
        })
    }

}