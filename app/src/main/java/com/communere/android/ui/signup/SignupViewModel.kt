package com.communere.android.ui.signup

import android.content.Context
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.communere.android.R
import com.communere.android.di.CoroutineScopeIO
import com.communere.android.data.repository.SignupRepository
import com.communere.android.data.model.Role
import com.communere.android.data.model.User
import com.communere.android.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import java.io.File
import javax.inject.Inject

/**
 * Interface between fragment and repository
 * @property context, for getting real path of selected image file and compressing it
 * @property repository, for providing data
 */
class SignupViewModel @Inject constructor(
    private val context: Context,
    private val repository: SignupRepository,
    @CoroutineScopeIO private val ioScope: CoroutineScope
) : ViewModel() {

    // For showing messages to user
    private val _message = MutableLiveData<Int>()
    val message: LiveData<Int> = _message

    // For showing state of registration
    private val _isRegistered = MutableLiveData<Boolean>()
    val isRegistered: LiveData<Boolean> = _isRegistered

    // For keeping a reference to selected image
    private val _image = MutableLiveData<String>()
    val image: LiveData<String> = _image

    fun signup(
        fullName: String?,
        email: String,
        password: String,
        confirmPassword: String,
        isAdmin: Boolean
    ) {

        // Check password and confirmation are matched
        if (password != confirmPassword) {
            _message.postValue(R.string.msg_password_confirmation_error)
            return
        }

        // Check email validation
        if (!email.isValidEmail()) {
            _message.postValue(R.string.msg_email_input_is_not_valid)
            return
        }

        // Check password formatting
        if (!password.isValidPassword()) {
            _message.postValue(R.string.msg_password_incorrect_format)
            return
        }

        // Create a new user
        val user = User(
            fullName = fullName,
            email = email,
            password = password,
            image = image.value,
            role = if (isAdmin) {
                Role.ADMIN
            } else {
                Role.REGULAR
            }
        )

        // Save to DB
        repository.signup(user, ioScope)

        // Post a message for showing successful registration
        _message.postValue(R.string.msg_signedup_successfully)

        // Post signup state
        _isRegistered.postValue(true)
    }

    /**
     * When user selected a file from intent.
     * Compress the file and move it to Pics dir.
     * @param data, given by intent
     */
    fun onImageSelected(data: Uri?) {
        // Init image property inside model
        val img = "${getRandomString()}.jpg"

        // Post new image selected
        _image.postValue(img)

        // Get image's file from data
        val actualImage = File(RealPathUtil.getRealPath(context, data))

        // compress image to lower size
        context.compressImageAndCopyToPics(actualImage, img)
    }

    /**
     * Cancel any process is running now
     */
    override fun onCleared() {
        ioScope.cancel()
        super.onCleared()
    }

}