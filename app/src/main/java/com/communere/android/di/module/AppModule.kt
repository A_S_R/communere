package com.communere.android.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.communere.android.data.PREFS_NAME
import com.communere.android.data.local.db.AppDatabase
import com.communere.android.data.local.prefs.PrefsUtils
import com.communere.android.di.CoroutineScopeIO
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Suppress("unused")
@Module(includes = [ViewModelModule::class])
class AppModule {

    /**
     * Provides app database.
     * @param app, an instance of application class
     */
    @Singleton
    @Provides
    fun provideDb(app: Application) = AppDatabase.getInstance(app)

    /**
     * Provides an instance of [PrefsUtils] class.
     * @param sharedPreferences
     * @return instance of PrefsUtils
     */
    @Singleton
    @Provides
    fun providePrefsHelper(sharedPreferences: SharedPreferences): PrefsUtils {
        return PrefsUtils(
            sharedPreferences
        )
    }

    /**
     * Provides an instance of [SharedPreferences].
     * @param context
     */
    @Provides
    fun provideSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    /**
     * Provides application context.
     * @param app, instance of [Application]
     */
    @Singleton
    @Provides
    fun provideContext(app: Application): Context = app.applicationContext

    /**
     * Provides an IO scope of [Dispatchers]
     */
    @CoroutineScopeIO
    @Provides
    fun provideCoroutineScopeIO() = CoroutineScope(Dispatchers.IO)

    /**
     * TODO: We'll add any new Dao here, like below
     ****************************** BEGINS ********************************
     */

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) = db.userDao()

    /**
     ***************************** ENDS *********************************
     */
}
