package com.communere.android.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.communere.android.ui.base.ViewModelFactory
import com.communere.android.di.ViewModelKey
import com.communere.android.ui.login.LoginViewModel
import com.communere.android.ui.signup.SignupViewModel
import com.communere.android.ui.user.ProfileViewModel
import com.communere.android.ui.user.UsersViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * This class provides view models instance.
 */
@Suppress("unused")
@Module
abstract class ViewModelModule {

    /**
     * TODO: We'll add any new ViewModel in whole project like above
     */

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignupViewModel::class)
    abstract fun bindSignupViewModel(viewModel: SignupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UsersViewModel::class)
    abstract fun bindUsersViewModel(viewModel: UsersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
