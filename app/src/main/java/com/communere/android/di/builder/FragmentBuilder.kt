package com.communere.android.di.builder

import com.communere.android.ui.login.LoginFragment
import com.communere.android.ui.signup.SignupFragment
import com.communere.android.ui.user.ProfileFragment
import com.communere.android.ui.user.UsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Fragment contributes for injection of app component, defined here
 */
@Suppress("unused")
@Module
abstract class FragmentBuilder {

    /**
     * TODO: We'll add any new Fragment in whole project like above
     */

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeSignupFragment(): SignupFragment

    @ContributesAndroidInjector
    abstract fun contributeUsersFragment(): UsersFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

}
