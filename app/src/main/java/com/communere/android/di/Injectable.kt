package com.communere.android.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
