package com.communere.android.data.model

/**
 * Keep constant values of roles, defined in app
 */
object Role {
    const val ADMIN = "admin"
    const val REGULAR = "regular"
}