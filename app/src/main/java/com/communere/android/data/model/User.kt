package com.communere.android.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * User model
 */
@Parcelize
@Entity(tableName = "tbl_users")
data class User(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,

    var fullName: String? = null,

    var email: String? = null,

    var password: String? = null,

    var image: String? = null,

    var role: String? = null
) : Parcelable