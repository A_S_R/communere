package com.communere.android.data.repository

import androidx.lifecycle.liveData
import com.communere.android.data.local.db.dao.UserDao
import com.communere.android.data.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Provides data for viewmodel
 * @param dao, executes transaction on table
 */
@Singleton
class UserRepository @Inject constructor(private val dao: UserDao) {

    val users = dao.getUsers()

    fun getUserById(id: Int) = liveData(Dispatchers.IO) {
        emitSource(dao.getUserById(id))
    }

    fun update(user: User, ioScope: CoroutineScope) {
        ioScope.launch { dao.upsert(user) }
    }

    fun delete(user: User, ioScope: CoroutineScope) {
        ioScope.launch { dao.delete(user) }
    }

}