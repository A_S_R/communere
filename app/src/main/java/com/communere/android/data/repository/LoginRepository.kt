package com.communere.android.data.repository

import androidx.lifecycle.liveData
import com.communere.android.data.local.db.dao.UserDao
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Provides data for ViewModel
 * @property dao, for executing transaction to table
 */
@Singleton
class LoginRepository @Inject constructor(private val dao: UserDao) {

    /**
     * Finds user in tbl_users
     * @param email, user's email
     * @param password, user's password
     * @return LiveData<User>
     */
    fun login(email: String?, password: String?) = liveData(Dispatchers.IO) {
        emitSource(dao.login(email, password))
    }

}