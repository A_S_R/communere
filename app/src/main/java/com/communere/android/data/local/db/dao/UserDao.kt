package com.communere.android.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.communere.android.data.model.User

/**
 * Handles transactions to tbl_users
 */
@Dao
interface UserDao {

    @Query("SELECT * FROM tbl_users ORDER BY fullName DESC")
    fun getUsers(): LiveData<List<User>>

    @Query("SELECT * FROM tbl_users WHERE id=:id")
    fun getUserById(id: Int): LiveData<User>

    @Query("SELECT * FROM tbl_users WHERE email=:email AND password=:password")
    fun login(email: String?, password: String?): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(user: User)

    @Delete
    suspend fun delete(user: User)

}