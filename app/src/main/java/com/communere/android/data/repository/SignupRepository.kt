package com.communere.android.data.repository

import com.communere.android.data.model.User
import com.communere.android.data.local.db.dao.UserDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Provides data for ViewModel
 * @property dao, for executing transaction to table
 */
@Singleton
class SignupRepository @Inject constructor(private val dao: UserDao) {

    /**
     * Register user to tbl_users
     * @param user, instance of User
     * @param ioScope, for doing transaction on an io thread
     */
    fun signup(user: User, ioScope: CoroutineScope) = ioScope.launch {
        dao.upsert(user)
    }

}