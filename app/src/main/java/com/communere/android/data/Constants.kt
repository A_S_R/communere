package com.communere.android.data

/**
 * We define any constants of the whole app here.
 */

const val PREFS_NAME = "COMMUNERE_PREFS"
const val DIR_PICS = "pictures"