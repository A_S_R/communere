package com.communere.android.utils

import androidx.core.content.FileProvider

/**
 * We need to provide a custom file provider for the app
 */
class GenericFileProvider: FileProvider()