package com.communere.android.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.communere.android.R
import com.communere.android.utils.getPicsDir
import com.squareup.picasso.Picasso
import java.io.File

/**
 * Handles visibility of views
 * @param view, the view has been provided
 * @param isVisible, condition for visibility
 */
@BindingAdapter("isVisible")
fun bindIsVisible(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

/**
 * Handles showing user's profile image
 * from files directory
 * @param view, the target image view
 * @param fileName, file name in directory
 */
@BindingAdapter("imageProfile")
fun bindImageProfile(view: ImageView, fileName: String?) {
    if (!fileName.isNullOrEmpty()) {
        Picasso.get()
            .load(File(view.context.getPicsDir(), fileName))
            .placeholder(R.drawable.ic_account_circle)
            .fit().centerCrop()
            .into(view)
    }
}


