@file:Suppress("unused")
package com.communere.android.utils

import android.content.Intent
import android.content.pm.PackageManager
import android.provider.MediaStore
import android.view.View
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar

/**
 * Shows a snack message to user
 * @param message, the message we want to show (from strings.xml)
 * @param action, the action name of the message (from strings.xml)
 * @param function, if user clicks on the action this function will execute
 */
fun View.showSnack(
    @StringRes message: Int,
    @StringRes action: Int? = null,
    function: (() -> Unit)? = null
) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    if (action != null && function != null) {
        snackbar.setAction(action) { function() }
    }
    snackbar.show()
}

/**
 * Shows a snack message to user
 * @param message, the message we want to show (String)
 * @param action, the action name of the message (String)
 * @param function, if user clicks on the action this function will execute
 */
fun View.showSnack(
    message: String,
    @StringRes action: Int? = null,
    function: (() -> Unit)? = null
) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    if (action != null && function != null) {
        snackbar.setAction(action) { function() }
    }
    snackbar.show()
}

/**
 * Checks user has a specific permission or not
 * @param permission, the permission we want to request
 * @param requestCode, for handling result of permission request inside of fragment
 * @param function, do this function if the user has the granted permission
 */
fun Fragment.hasPermission(permission: String, requestCode: Int, function: () -> Unit) {

    val hasPermission = ContextCompat.checkSelfPermission(
        requireContext(),
        permission
    ) == PackageManager.PERMISSION_GRANTED

    if (hasPermission) {
        function()
    } else {
        requestPermissions(arrayOf(permission), requestCode)
    }
}

/**
 * Opens gallery intent or file manager for selecting an image
 * @param requestCode, for handling result of intent inside of fragment
 */
fun Fragment?.openGallery(requestCode: Int) {
    this ?: return

    val getIntent = Intent(Intent.ACTION_GET_CONTENT)
    getIntent.type = "image/*"

    val pickIntent = Intent(
        Intent.ACTION_PICK,
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    )
    pickIntent.type = "image/*"

    val chooserIntent = Intent.createChooser(getIntent, "Choose Image By:")
    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

    startActivityForResult(chooserIntent, requestCode)
}