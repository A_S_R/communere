package com.communere.android.utils

import android.content.Context
import android.graphics.Bitmap
import android.util.Patterns
import com.communere.android.data.DIR_PICS
import id.zelory.compressor.Compressor
import java.io.File
import java.io.IOException
import java.util.regex.Pattern

/**
 * Returns pics directory of app
 * @return null, if the maintainer context is null
 * otherwise directory of pics as a [File]
 */
fun Context?.getPicsDir(): File? {
    this ?: return null

    val file = File(
        getExternalFilesDir(null),
        DIR_PICS
    )

    if (file.exists().not()) {
        file.mkdirs()
    }

    return file
}

/**
 * Compress an image file
 * @param actualImage, the original image file
 * @param compressedName, name of the compressed image file
 * @return compressed image file as a [File]
 */
fun Context?.compressImageAndCopyToPics(actualImage: File, compressedName: String): File? {
    return try {
        val file = File(getPicsDir(), compressedName)
        if (file.exists()) file.delete()

        return Compressor(this)
            .setCompressFormat(Bitmap.CompressFormat.JPEG)
            .setDestinationDirectoryPath(getPicsDir()?.absolutePath)
            .compressToFile(actualImage, compressedName)
    } catch (e: IOException) {
        e.printStackTrace()
        null
    }
}

/**
 * Checks the given [CharSequence] for email format
 * @return true, if it has the correct email format,
 * false, if it has NOT.
 */
fun CharSequence?.isValidEmail() =
    !this.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

/**
 * Checks the given [CharSequence] for password validation
 * @return true, if it has numerical and capital letters
 * inside it, false, if it has NOT.
 */
fun CharSequence?.isValidPassword(): Boolean {

    this ?: return false

    val regex =
        "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$"

    return Pattern.compile(regex).matcher(this).matches()
}

/**
 * @return a random string with length of 15.
 */
fun getRandomString(): String {
    val allowedChars = ('A'..'Z') + ('a'..'z')
    return (1..15)
        .map { allowedChars.random() }
        .joinToString("")
}